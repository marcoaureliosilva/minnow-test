const request = require('supertest');
const { server } = require('../../lib/api');
const { TeamsRepository } = require('../../lib/api/teams/repository');

jest.mock('../../lib/api/teams/repository', () => {
  return {
    TeamsRepository: jest.fn().mockImplementation(() => {
      return {
        getByMinimumPosition: () => [
          {
            team: 'Bayern Munich',
            position: 1,
            league: 'ger.1',
          },
          {
            team: 'Borussia Dortmund',
            position: 2,
            league: 'ger.1',
          },
          {
            team: 'Bayer Leverkusen',
            position: 3,
            league: 'ger.1',
          },
          {
            team: 'SC Freiburg',
            position: 4,
            league: 'ger.1',
          },
        ],
        getByMaximumPosition: () => [
          {
            team: 'FC Augsburg',
            position: 16,
            league: 'ger.1',
          },
          {
            team: 'Arminia Bielefeld',
            position: 17,
            league: 'ger.1',
          },
          {
            team: 'SpVgg Greuther Furth',
            position: 18,
            league: 'ger.1',
          },
        ],
      };
    }),
  };
});

describe('Test all 4 routes available', () => {
  test('It should return the top 4 teams', done => {
    request(server)
      .get('/teams/germ.1/top/4')
      .then(response => {
        expect(response.statusCode).toBe(200);
        expect(response.body).toStrictEqual({
          status: 'success',
          result: [
            {
              team: 'Bayern Munich',
              position: 1,
              league: 'ger.1',
            },
            {
              team: 'Borussia Dortmund',
              position: 2,
              league: 'ger.1',
            },
            {
              team: 'Bayer Leverkusen',
              position: 3,
              league: 'ger.1',
            },
            {
              team: 'SC Freiburg',
              position: 4,
              league: 'ger.1',
            },
          ],
        });
        expect();
        done();
      });
  });
  test('It should bring the relegation teams', done => {
    request(server)
      .get('/teams/germ.1/relegation')
      .then(response => {
        expect(response.statusCode).toBe(200);
        expect(response.body).toStrictEqual({
          status: 'success',
          result: [
            {
              team: 'FC Augsburg',
              position: 16,
              league: 'ger.1',
            },
            {
              team: 'Arminia Bielefeld',
              position: 17,
              league: 'ger.1',
            },
            {
              team: 'SpVgg Greuther Furth',
              position: 18,
              league: 'ger.1',
            },
          ],
        });
        expect();
        done();
      });
  });
  test('It should bring the available leagues', done => {
    request(server)
      .get('/leagues')
      .then(response => {
        expect(response.statusCode).toBe(200);
        done();
      });
  });
});
