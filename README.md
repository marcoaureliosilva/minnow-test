# minnow-test

Minnow Test Assignment

## Requirements

An AWS Account

## Getting started

```bash
git clone https://gitlab.com/marcoaureliosilva/minnow-test
cd minnow-test
npm i
```

## Running locally

On project root folder (minnow-test):

```bash
AWS_ACCESS_KEY_ID=$YOUR_ACCESS_KEY_ID AWS_SECRET_ACCESS_KEY=$YOUR_SECRET_ACCESS_KEY node lib/local.js
```

## Test

On project root folder (minnow-test):

```bash
npm test
```

## Deploy

On project root folder (minnow-test):

```bash
sls deploy -s dev
```

or to any stage (must have .env.$state.yml env file on root folder)

```bash
sls deploy -s $stage
```

---

# Considerations

Some things have not been fully finished in this test. It would be cases for future enhancements, they are:

1. Fully local dev environment using Docker
1. Test coverage > 90%
1. Finish "On Fire Teams" implementation
1. Use Inversion of Control through depency injection, once the api was built with that in mind
