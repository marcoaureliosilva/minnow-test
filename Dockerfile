FROM node:14-alpine

RUN npm install -g serverless

WORKDIR /app
COPY ./package.json ./package-lock.* /app/

RUN npm install
