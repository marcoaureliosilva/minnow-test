const { default: axios } = require('axios');
const { DynamoDB } = require('aws-sdk');
const { LeaguesService } = require('../api/leagues/service');

const STANDINGS_API_URL = process.env.STANDINGS_API_URL;
const db = new DynamoDB.DocumentClient({
  region: process.env.ENV_AWS_REGION,
});

async function upsertEntry(entry) {
  const TEAMS_TABLE_NAME = process.env.TEAMS_TABLE_NAME;
  return new Promise((resolve, reject) => {
    return db.put(
      {
        TableName: TEAMS_TABLE_NAME,
        Item: entry,
      },
      (error, data) => {
        console.error(error);
        if (error) return reject(error);

        return resolve(data.Item);
      },
    );
  });
}

async function fetchStandings() {
  try {
    const service = new LeaguesService();
    const leagues = await service.getAvailableLeagues();

    const standingsResponse = (
      await Promise.allSettled(
        leagues.map(league =>
          axios
            .get(
              `${STANDINGS_API_URL}/${league}/standings?season=2021&sort=asc`,
            )
            .then(response => {
              return {
                league: league,
                standings:
                  response.data &&
                  response.data.data &&
                  response.data.data.standings,
              };
            }),
        ),
      )
    )
      .filter(result => result.status === 'fulfilled')
      .map(result => result.value);

    const leagueEntries = standingsResponse.map(response =>
      response.standings.map((position, i) => ({
        league: response.league,
        position: (position.note && position.note.rank) || i + 1,
        team: position.team.name,
      })),
    );

    for (const leagueEntry of leagueEntries) {
      await Promise.allSettled(leagueEntry.map(entry => upsertEntry(entry)));
    }

    return leagueEntries;
  } catch (error) {
    console.error('Fetch standings failed', error);
  }
}

module.exports.fetchStandings = fetchStandings;
