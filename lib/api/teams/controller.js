class TeamsController {
  constructor(service) {
    this.service = service;
  }

  async getTopTeams(req, res) {
    try {
      const { league, number: position } = req.params;
      const result = await this.service.getTopTeams(league, position);
      res.json({
        status: 'success',
        result,
      });
    } catch (error) {
      res.status(500).json({ status: 'error', message: 'Uncaugh error' });
      console.error(error);
    }
  }

  async getRelegationTeams(req, res) {
    try {
      const { league } = req.params;
      const result = await this.service.getRelegationTeams(league);
      res.json({
        status: 'success',
        result,
      });
    } catch (error) {
      res.status(500).json({ status: 'error', message: 'Uncaugh error' });
      console.error(error);
    }
  }

  async getOnFireTeams(req, res) {
    try {
      const { league } = req.params;
      const result = await this.service.getOnFireTeams(league);
      res.json({
        status: 'success',
        result,
      });
    } catch (error) {
      res.status(500).json({ status: 'error', message: 'Uncaugh error' });
      console.error(error);
    }
  }
}

module.exports.TeamsController = TeamsController;
