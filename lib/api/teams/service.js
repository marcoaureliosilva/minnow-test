class TeamsService {
  relegations = {
    'fra.1': 18,
    'ger.1': 16,
    'eng.1': 18,
    'esp.1': 18,
    'ita.1': 18,
  };
  availableLeagues = ['fra.1', 'ger.1', 'eng.1', 'esp.1', 'ita.1'];
  constructor(repo) {
    this.repo = repo;
  }

  async getTopTeams(league, position) {
    return this.repo.getByMinimumPosition(league, position);
  }

  async getRelegationTeams(league) {
    return this.repo.getByMaximumPosition(
      league,
      this.relegations[league] || 16,
    );
  }

  async getOnFireTeams(league) {
    return this.repo.getByWinStreak(league);
  }
}

module.exports.TeamsService = TeamsService;
