const { Router } = require('express');
const { TeamsController } = require('./controller');
const { TeamsService } = require('./service');
const { TeamsRepository } = require('./repository');

const router = Router();
const controller = new TeamsController(new TeamsService(new TeamsRepository()));

router.get(
  '/teams/:league/top/:number',
  controller.getTopTeams.bind(controller),
);
router.get(
  '/teams/:league/relegation',
  controller.getRelegationTeams.bind(controller),
);
router.get(
  '/teams/:league/on-fire',
  controller.getOnFireTeams.bind(controller),
);

module.exports = router;
