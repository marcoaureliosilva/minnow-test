const { DynamoDB } = require('aws-sdk');

const db = new DynamoDB.DocumentClient({
  region: process.env.ENV_AWS_REGION,
});

class TeamsRepository {
  tableName = process.env.TEAMS_TABLE_NAME;
  defaultOnFireWinStreak = Number(process.env.DEFAULT_ON_FIRE_WIN_STREAK);
  defaultMinimumPosition = Number(process.env.DEFAULT_MINIMUM_POSITION);
  defaultMaximumPosition = Number(process.env.DEFAULT_MAXIMUM_POSITION);
  async getByMinimumPosition(
    league,
    minimumPosition = this.defaultMinimumPosition,
  ) {
    if (minimumPosition < 1) {
      minimumPosition = 1;
    }

    if (minimumPosition > 6) {
      minimumPosition = 6;
    }

    return new Promise((resolve, reject) => {
      db.query(
        {
          TableName: this.tableName,
          KeyConditionExpression: '#league = :league and #pos <= :pos',
          ExpressionAttributeNames: {
            '#league': 'league',
            '#pos': 'position',
          },
          ExpressionAttributeValues: {
            ':league': league,
            ':pos': Number(minimumPosition),
          },
        },
        (error, data) => {
          if (error) {
            console.error(error);
            return reject(error);
          }

          resolve(data.Items);
        },
      );
    });
  }

  async getByMaximumPosition(
    league,
    maximumPosition = this.defaultMaximumPosition,
  ) {
    if (maximumPosition > 20) {
      maximumPosition = 20;
    }

    return new Promise((resolve, reject) => {
      db.query(
        {
          TableName: this.tableName,
          KeyConditionExpression: '#league = :league and #pos >= :pos',
          ExpressionAttributeNames: {
            '#league': 'league',
            '#pos': 'position',
          },
          ExpressionAttributeValues: {
            ':league': league,
            ':pos': maximumPosition,
          },
        },
        (error, data) => {
          if (error) {
            console.error(error);
            return reject(error);
          }

          resolve(data.Items);
        },
      );
    });
  }

  getByWinStreak(league, streak = this.onFireWinStreak) {
    if (streak < 2) {
      streak = 2;
    }

    return new Promise((resolve, reject) => {
      db.query(
        {
          TableName: this.tableName,
          IndexName: 'WinStreakGSI',
          KeyConditionExpression:
            '#league = :league and winStreak >= :winStreak',
          ExpressionAttributeNames: {
            '#league': 'league',
          },
          ExpressionAttributeValues: {
            ':league': league,
            ':winStreak': streak,
          },
        },
        (error, data) => {
          if (error) {
            console.error(error);
            return reject(error);
          }

          resolve(data.Items);
        },
      );
    });
  }
}

module.exports.TeamsRepository = TeamsRepository;
