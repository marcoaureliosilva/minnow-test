const express = require('express');
const cors = require('cors');
const teams = require('./teams');
const leagues = require('./leagues');

const app = express();

app.use(cors());
app.use(express.json());
app.use(teams);
app.use(leagues);

module.exports.server = app;
