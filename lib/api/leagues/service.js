class LeaguesService {
  availableLeagues = ['fra.1', 'ger.1', 'eng.1', 'esp.1', 'ita.1'];

  async getAvailableLeagues() {
    return this.availableLeagues;
  }
}

module.exports.LeaguesService = LeaguesService;
