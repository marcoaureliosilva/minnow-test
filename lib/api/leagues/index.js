const { Router } = require('express');
const { LeaguesController } = require('./controller');
const { LeaguesService } = require('./service');

const router = Router();
const controller = new LeaguesController(new LeaguesService());

router.get('/leagues', controller.getAvailableLeagues.bind(controller));

module.exports = router;
