class LeaguesController {
  constructor(service) {
    this.service = service;
  }

  async getAvailableLeagues(_req, res) {
    try {
      const result = await this.service.getAvailableLeagues();
      res.json({
        status: 'success',
        result,
      });
    } catch (error) {
      res.status(500).json({ status: 'error', message: 'Uncaugh error' });
    }
  }
}

module.exports.LeaguesController = LeaguesController;
