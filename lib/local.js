require('dotenv-yaml').config({ path: __dirname + '/../.env.dev.yml' });
const { server } = require('../lib/api');
server.listen(3000, () => console.log('API running on port 3000'));
