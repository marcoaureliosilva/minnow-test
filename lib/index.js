const serverless = require('serverless-http');
const { server } = require('./api');
const { fetchStandings } = require('./sync');

module.exports.api = serverless(server);
module.exports.sync = fetchStandings;
